# Additional notes for RFC-011: News & announcements on site [WITHDRAWN]

This is an archive of discussions from the merge request, roughly organized but otherwise relatively raw (as signficant time shouldn't be spent on supplementary tasks like this).

## Thread started by Alyssa Rock - 27 Aug 2022

In general, this seems like a good idea and I'm tentatively in favor of it.

I have a couple of questions and comments:

**Question 1: User stories**

Could you tell me a little more about the user story for this initiative? I imagine the target persona is something like what I was calling "Casual Follower Casey":

![Casual Casey diagram 1](Attachments/rfc-011-image-casey-1.png)

![Casual Casey diagram 2](Attachments/rfc-011-image-casey-2.png)

Am I right that Casey (or something similar) is the kind of persona you'd be targeting here and the use case is that someone who just wants to keep tabs on the project because they might be an interested user of the project's output down the road? Are there any other additional use cases? What are the larger benefits and impacts to the project and to our community?

**Question 2: Technical feature requirements**

From a technical perspective, would you like to have the news on some sort of RSS feed that users could subscribe to and possibly have the sidebar that shows the 5 latest news stories on the homepage (or wherever you'd like it)? Would this feed also incorporate our Twitter feed somehow or how would it relate to that? Would there be some sort of email subscription that includes a news digest as well?

**Question 3: Press page**

We currently have a press page on our website: https://thegooddocsproject.dev/press/

This page is not terribly well-maintained and I'm slightly unclear what purpose that press page serves. Would your news page replace this content? Supplement this content?

**Concern 1: Team/project bandwidth**

I have one concern, but I don't consider this to necessarily be a blocking concern. I think I was primed to think about this concern after reading your RFC 13. 😜
My concern is around the project bandwidth for the News Team. You mentioned that this will be a separate team from the Blog team and from the Content Strategy team. I'm concerned about spinning up another workgroup/meeting cadence, especially when a lot of the people who would likely make up the News Team would overlap with the Blog WG and the Content Strategy WG. Can you estimate what the level of effort will be required for this project and its relationship to those other working groups? Would it maybe be instead possible to fold this work into the Content Strategy group? If so, would that team feel confident they could take this initiative on?

**Concern 2: Conflicting dependencies on future Content Strategy decisions**

I have a mild concern that if the tech team sets to work on building out this news feature for you that it could possibly be undone by work that the Content Strategy team is doing. I haven't been to Content Strategy meetings in quite some time (that's my bad!), but I thought I remembered that team possibly taking on a spike where they were going to evaluate other Hugo themes for our website that could better meet our website's needs. If we were going to switch Hugo themes, I'm mildly concerned that it would impact the ability to feature a news feed. But I'm not sure.

**Praise**

Nice work on this, Ryan! I look forward to hearing more discussion from other community members as well. ❤

### \[reply\] Ryan - 29 Aug 2022

> **Question 1: User stories**
> 
> Could you tell me a little more about the user story for this initiative? I imagine the target persona is something like what I was calling "Casual Follower Casey":

That's the most significant persona to target, one that I feel will have spikes at WTD (and eventually other) conferences, as their interest in the project in general could draw them to a news post either from looking over the site, or from looking our Twitter etc. So I'd say we have the variant of Casey we're addressing: "Just-Found-Out-About-the-Project Justine."

> **Question 2: Technical feature requirements**
> 
> From a technical perspective, would you like to have the news on some sort of RSS feed that users could subscribe to and possibly have the sidebar that shows the 5 latest news stories on the homepage (or wherever you'd like it)? Would this feed also incorporate our Twitter feed somehow or how would it relate to that? Would there be some sort of email subscription that includes a news digest as well?

* RSS feed: We could. I'd leave that as a tech team/content strategy decision. I don't have a strong opinion here. That could be a defered decision.
* Sidebar: That gets into a content strategy conversation, about how to best display such stuff on the site overall, so I'd defer that decision as well.
* Right now, our Twitter is manually managed. Posting to Twitter (and what other social media we end up doing) would be part of the process, but it's manual rather than technical.
* Email: We could do that, and I see the content as being possible cross-used, but I think "have an email list" is a separate decision

> **Question 3: Press page**
> 
> We currently have a press page on our website: https://thegooddocsproject.dev/press/
> 
> This page is not terribly well-maintained and I'm slightly unclear what purpose that press page serves. Would your news page replace this content? Supplement this content?

That seems like a disorganized list of links where people have talked about the project elsewhere—more like a CV than something useful. I recommend scrapping that. We can repurpose individual elements as needed, but it doesn't seem to really give Casual Casey any genuine direction.

> **Concern 1: Team/project bandwidth**
> 
> I have one concern, but I don't consider this to necessarily be a blocking concern. I think I was primed to think about this concern after reading your RFC 13. 😜
My concern is around the project bandwidth for the News Team. You mentioned that this will be a separate team from the Blog team and from the Content Strategy team. I'm concerned about spinning up another workgroup/meeting cadence, especially when a lot of the people who would likely make up the News Team would overlap with the Blog WG and the Content Strategy WG. Can you estimate what the level of effort will be required for this project and its relationship to those other working groups? Would it maybe be instead possible to fold this work into the Content Strategy group? If so, would that team feel confident they could take this initiative on?

Overall: This could live within the blog team & social media team (which already has overlap), especially if the archtecture was similar enough. It's my understandig that the content strategy team would end up dissolving effectively once the strategy is figured out and implemented.

Re meetings: News would be done as-needed, rather than the blog's goal of periodic content (albeit now less frequent than the prior ambition). If someone needs something posted, like we're having an upcoming release or want to announce a talk by a community member, that spins up an ask. No need for a separate proactive, always-on group.

The effort level after site instantiation would be small, a few posts a year. We don't want to overwhelm people with "not really news," and want to make it easy for people to find prior-but-still-relevant news posts.

> **Concern 2: Conflicting dependencies on future Content Strategy decisions**
> 
> I have a mild concern that if the tech team sets to work on building out this news feature for you that it could possibly be undone by work that the Content Strategy team is doing. I haven't been to Content Strategy meetings in quite some time (that's my bad!), but I thought I remembered that team possibly taking on a spike where they were going to evaluate other Hugo themes for our website that could better meet our website's needs. If we were going to switch Hugo themes, I'm mildly concerned that it would impact the ability to feature a news feed. But I'm not sure.

Whatever work is being done regarding Hugo shifts and implementations should definitely block this work. If this idea overall has merit enough to pass, but needs other strategy/tech decisions and implementations to happen, then we hold off on implementing this until then. (And allow this to factor into such architectural planning.)

## Felicity Brand - 1 Sep 2022

Thanks for writing this up @ryanmacklin. I'm all in on this 💯

I don't want to over-engineer it. Do we really need RSS? Do we need newsletter subscription? I don't think so. Maybe Q4 next year when we're really releasing stuff folk might actually want to be informed (via push) about what we're doing. Of course, let's make wise decisions now to be future-proof, but I wouldn't want to slow implementation of this because we're faffing with fancy stuff we don't need.

We're not so heavy on news yet that we need to adjust our currently manual tweeting.

I see writing news as very similar to blog posts, so in terms of effort, no big deal. Since we're slowing the blog at the moment, and focussing on project news, it makes total sense to write some news for a while.

I am also personally keen to do advocacy for the project (talk about it publicly), and having news to point to works in really well for that.

### \[reply\] Ryan - 1 Sep 2022

I'll make a note splitting "content" and "presentation." I'll detail a bit more about the intended content/make sure it's easy to parse, and flag "presentation" as a Content Strategy decision to defer, since it sounds like we could use flexibility in their presentation models.

## Carrie Crowe - 1 Sep 2022

This is excellent and seems like we are on the same track. Happy to provide more details soon.

## Ryan Macklin - 1 Sep 2022

Per conversations in today's CS meeting: it sounds like the CS team will handle news & social media for the time being, \[sentenced dropped in original merge request\]

## Bryan Klein - 1 Sep 2022 \(in-file comments\)

> \[from file\] The posts should be short: 500 words or so. Something that would take on average less than 5 minutes to read. We may deviate from this as needed, but the core concept is that our newsletters shouldn't take significant resources to craft and review, and we shouldn't demand significant time expenses from our audience in this manner.

I'm starting to think of a micro-blogging format, or something with a significantly reduced character count. Like the newest Tweet length (280 chars) on Twitter. Enough to give a quick update about something and then link to more information, etc. One advantage of using a shorter char count would be the ability to take the syndication feed from these posts and automatically send them out to channels on Twitter, Facebook, etc., without rewriting them to fit.

### \[reply\] Ryan - 1 Sep 2022

I'd like it to not be that stringent. If the news has needs beyond micro-blogging, like (and hopefully this never happens) a public apology for a key member's conduct, we should make sure we can support that.

### \[reply\] Bryan - 2 Sep 2022

I don't think it need some kind of hard coded limit, but I would think that the use of this 'news' stream is for small, quick 'pings' of information from the team to remind people we are doing things and bring them up to speed. If we go over 280 chars, the content cannot be 'retweeted' so to speak without a bit of manual rewrite or truncation.

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

> \[from file\] Unlike how we handle the blog, there wouldn't be any commenting feature. 

But we can add some buttons to make it easy for people to share the posts on their own social media channels as needed.

> [\from file\] Unlike how we handle the blog, there wouldn't be any banner or hero images.

I'm thinking of something small like twitter cards for the layout.

### \[reply\] Ryan - 1 Sep 2022

One thing to consider: we could have a V1 and V2 plan, with the V2 being "fancier" as we end up feeling out this in practice, and V1 being what the CS team feel comfortable with when the new site is launched

One of the drafts I've seen from CS (seen today) has Twitter on the sidebar, so we might not want to use the same layout for two different elements. Though there are plenty of ways to implement "Twitter-like" that looks significantly different.

### \[reply\] Bryan - 2 Sep 2022

Yeah, the styles can be different, while conceptually the same. Small bits of information to inform viewers of project activities. With some clear visual separation from one to another.

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

> [\from file\] Because news is timely, the individual post URLs would use the date rather than a title.

This would not allow more than 1 post a day, unless we also add the time to the url. We could just generate an ISO datetime string for the slug. ISO datetime would probably be easiest and would provide some information in the string that would mean something about when the post was created.

### \[reply\] Ryan - 1 Sep 2022

That's a good point!

Ideally we don't want more than one news post a day, but to my earlier point about the potential need for a public apology (or something like an emergency rollback or whatever, should we for some reason need that—like if we get into doc tooling), we should make sure we can support multiple posts per day.

Better to support and never need than kick ourselves for not having it, right? As long as it doesn't turn into overengineering. (I'm okay to engineer for contingencies we need to support as the org grows, as long as they're reasonable to consider, and it's less costly to implement them now than later.)

### \[reply\] Bryan - 2 Sep 2022

We have to name the files something, might as well be a date/time code if we aren't using text slugs.

### \[reply\] Aaron - 7 Sep 2022

There's probably other valid reasons for multiple posts per day, e.g. what we're doing/have done during WTD conferences. I'd argue that just appending a "-2" or similar would be sufficient, but using a timestamp works too.

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

> [\from file\] Second, while the framework might be similar, it isn't exactly the same. So we can reuse a lot of the work on the blog, but it has different content needs.

I see a different 'content type' for the markdown files, and new layouts for this section of the site. Something like a 'twitter feed' with a card for each one.

### \[reply\] Ryan - 1 Sep 2022

Totally. I'll leave the markdown implementation as a post-RFC phase, since I'm sure like with the blog, we'll discover nuances as we build it out. But I'm def picking up what you're putting down.

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

This could also all be stored in a structured JSON data file, with a nice little editor for each entry in the file.

> [\from file\] If we want to get to a point of having a mailing list to keep our users informed, this is the content we'd use. And by having two categories, people could opt into different types of content. However, that isn't a requirement to execute on this idea.

It would be easy to create a 'filtered' set of posts from this data/file set for automated collection of updates in a newsletter.

We could also and probably should syndicate this to an RSS/Atom feed.

### \[reply\] Ryan - 1 Sep 2022

What's the time cost of that? And does the time cost change if we decide that's a "1.1" version of news?

### \[reply\] Bryan - 2 Sep 2022

It isn't much more time or effort, much easier than setting up a new content section and layouts for the website.

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

> [\from file\] This would be an additional effort set for the Tech Team, to take the blog and tweak it for another subfolder. That might involve refactoring material to be shared, or not—details on the code execution part of this effort is beyond the scope of the RFC, barring noting any concerns, issues, or plans that the Tech Team might share upon reviewing this.

Adding a new content type section or data file would be fairly simple, and making new layouts for the pages and content is fairly straightforward.

### \[reply\] Ryan - 1 Sep 2022

Yay! I figured that was the case, since my initial experience with Hugo working on the blog with you. I'm very thankful you commented to confirm that! :D

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

> [\from file\] **Open questions**

Would this be text/links only, or allow for media assets like images, videos, embeds, etc.?

### \[reply\] Ryan - 1 Sep 2022

Let's assume embeds, and have some guidelines for what those embeds should look like/size of them/etc.

But turning that around: what logistics do we need to consider on the tech side for that? (I believe I know, but I'd rather get specifics from you than silently assume)

### \[reply\] Bryan - 1 Sep 2022

There are some built in Hugo shortcodes that could come in handy here. https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes

> [\from file\] **Implementation checklist**

## Bryan Klein - 1 Sep 2022 \(in-file comments, cont.\)

**Add:** Define a new 'content type' that would have the necessary data structure for this kind of information object.

### \[reply\] Ryan - 1 Sep 2022

I'll make a task category of "technical implementations" and add this (and move "set up folder" etc.) in that. And make "Create process" as a task category.

## Tina Luedtke - 6 Sep 2022 \(in-file comments\)

> [\from file\] The news site would handle two types of content: announcements and heartbeats.

I agree with these types of content.

> [\from file\] Heartbeats could also possibly showcase an individual contributor, like a "meet the team" sort of vibe. This has been declined as a blog post topic, as that doesn't fit the purpose of the blog, but it could fit here at the tail end of heartbeat posts.
>
> (That also gives those individual contributors featured a different sort of call to action on publicizing their related post, as friends/colleagues of that contributor may be interest in the post for relationship reasons, then find the other content or project in general interesting enough to follow on its own.)

I love the social engineering aspect hehe.

> [\from file\] **Why not use the blog?**

> The blog is about "docs advocacy & education," so if peopple subscribe to the blog/look out for new posts, they should expect material that's education, not that's purely about our group marketing.

> [\from file\] The main page should show the most recent post, so when one clicks on "News" in the header, they don't have to click on a second link to see what's the most recent.

I agree with this sort of implementation.

For reference, this is how our implementation could look like:


![screenshot of Netdata's visual implementation](Attachments/rfc-011-image-netdata.png)

You can check it out at (Netdata Documentation)[https://learn.netdata.cloud/]

## Cameron Shorter - 10 Sep 2022

Praise: I don't feel strongly about this, but am tentatively supportive if we have volunteers wanting to drive this.
Suggest call out a couple of extra risks:

* Risk 1. There are way too many websites with a "news" section which hasn't had a new post for years. It just makes the site seem stale. Possible solution: Make the site resilient to having no updates for a while. For example, Don't call the section "news". Select another term, such as "updates", or "announcements" or similar.
* Risk 2. People push out low value content just to keep up a heartbeat cadence. If we don't have anything valuable to say, we shouldn't say anything.
* Risk 3. Spreading our community too thin by picking up another task.

Praise: I like the micro-content suggestion.

## Aaron - 7 Sep 2022

> [\from file\] Unlike how we handle the blog, authors wouldn't link to "about the author" pages as we do in the blog—and in fact we may decide there are no listed individual authors, to keep the news to appear to always be from the community.

This follows, I don't think there's a compelling reason to attach authors to these items, and them coming from the "community at large" has some benefits.


## Alyssa Rock - 2 Oct 2022

I'm voting +1 on this.

+1 vote from me is because:

* This news feature seems like it won't be too heavy of a burden people-wise or tech-wise.
* Bryan (representing the Tech Team here) has some early ideas about possible tech integrations that can achieve the desired goals.
* It sounds like the Content Strategy team is aligned and on-board.

Thanks, everyone!

## Carrie Crown - 26 Oct 2022

Great work on this RFC.

I'm voting +1 on this.

+1 vote from me is because:

* I like the idea of micro-content - adding updates or announcements when needing. A place to share that as a group we attended conferences like WTD without having a blog post dedicated to it.
* Later, a news section will helpful to us when we do have template releases.
* This seems like not too big of technical lift from what Bryan is describing.

## End of archived discussion
