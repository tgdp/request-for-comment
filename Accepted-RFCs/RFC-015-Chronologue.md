# Chronologue processes and tools

## Proposed by

[Tina Luedtke](https://thegooddocs.slack.com/team/U02EQQDFLE8)

## Current status

- [x] Draft
- [x] Under discussion
- [x] Final comment and voting (until 2022-12-01)
- [ ] Accepted
- [ ] Rejected
- [ ] Implemented
- [ ] Deferred
- [ ] Withdrawn


## Proposal overview

This proposal outlines the responsibilities, ambitions, needs, and ways of working within the Chronologue working group and how we interact with the Template working group. Specifically, the proposal discusses: 

-   [Function of the Chronologue project](#function)
-   [Role Matrix](#roles)
-   [Writing process](#process)
-   [Mock product: Technical and creative needs and a proposal for tooling](#tools)



## Motivation

This proposal aims to solidify our past decisions into a formal foundation from which the group can evolve and grow.  
More specifically, the proposal highlights the role of the Chronologue working group within the Good Docs project, how we work, and what tools we use.

## Proposal

### Function of the Chronologue project <a id="function"> </a>

The Chronologue project is a working group within the Good Docs project. 
The primary task of the group is to create example documentation to increase the understanding of what good documentation can look like. 
The primary purpose is to educate people without a background in technical communication.
The group uses the Good Docs Project’s templates to create these examples, so the secondary purpose of the group is to act as a quality assurance for the Template working group.

To create credible examples, the group decided to build a fictional product - the Chronologue. The Chronologue is a time-travel telescope that records astronomical events of the future, and past. 

The Chronologue consists of:  

-   A time-travel telescope (fictional)
-   An API to transmit data between the telescope and the website
-   A website to view events recorded by the telescope


### Role Matrix <a id="roles"> </a>

This section describes roles within Chronologue and what they typically do. 
Members should take on one base role, and if bandwidth permits, they should take on a vertical role as well. 

> Context: In the past, the working group lead took on a lot of organizational tasks, which wasn't a sustainable workload. 
> Shifting to a more distributed model helps build a sustainable and meaningful experience for everyone. 

#### Base roles
- Working group lead (Lead) - Group representative that coordinates members, leads release planning, and ensures progress towards set goals.
- Individual contributor (IC) - Member that primarily writes code or documentation to make progress towards goals defined by the lead. 

#### Vertical roles

> Some of the roles might be needed for a limited time, others might be needed on an ongoing basis

- Communication point with the larger project (ExtComms)- Attending the general US meeting to give updates on the Chronologue project.
- Internal documentation (IntComms) - Finding the gaps in internal docs in our current repos. Takes initiative to create internal docs, but doesn't have to do all on their own.
- Meeting management (MM) - Setting Chronologue meeting agendas and running the meetings.
- Project management (PM) - Coordinating GitLab issues, making sure that people get assigned, and tracking progress. Checking group members for status updates and making sure it's reflected in issues or other internal documentation.
- Onboarding -  Helping new Chronologue members choose their first task and get started.
Coaching and support (Coaching) - When Chronologue members get stuck or need assistance, giving them advice or support to become unblocked.
- Process - Defining how the group works in itself and with cross-functional stakeholders (primarily template group)

#### Role summary

The table is organized as a [RACI chart](https://www.teamgantt.com/blog/raci-chart-definition-tips-and-example).
RACI is an acronym that stands for **R**esponsible, **A**ccountable, **C**onsulted, **I**nformed. 
Each letter in RACI represents a level of task responsibility on a project.



<table>
  <tr>
   <td>Role→ <br> Task↓</td><td>Lead</td><td>IC</td><td>ExtComms</td><td>IntComms</td><td>MM</td><td>PM</td><td>Onboarding</td><td>Coaching</td><td>Process</td></tr>
  <tr>
   <td>Running Chronologue meetings</td><td>A</td><td>C</td><td>C</td><td>I</td><td>R</td><td>I</td><td>I</td><td>I</td><td>C</td></tr><tr>
   <td>Onboarding new members
   </td><td>A</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>R</td><td>C</td><td>I</td></tr>
  <tr>
   <td>Release planning</td><td>A, R</td><td>R</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>C</td></tr>
  <tr>
   <td>Creating & maintaining the documentation plan</td><td>A</td><td>C</td><td>I</td><td>C</td><td>I</td><td>R</td><td>I</td><td>I</td><td>I</td></tr>
  <tr>
   <td>Assigning tasks within the group</td><td>A, R</td><td>C</td><td>I</td><td>I</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td></tr>
  <tr><td>Writing Chronologue documentation</td><td>A</td><td>R</td><td>I</td><td>I</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td>
  </tr><tr>
   <td>Web development</td><td>A</td><td>R</td><td>I</td><td>C</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td></tr>
<tr>
   <td>Maintaining docs tools</td><td>A</td><td>R</td><td>I</td><td>C</td><td>I</td><td>C</td><td>I</td><td>-</td><td>I</td>
  </tr>
  <tr>
   <td>Sharing Chronologue progress externally</td><td>C</td><td>C</td><td>A, R</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td>
  </tr>
  <tr><td>Creating internal documentation</td><td>R</td><td>R</td><td>I</td><td>A, R</td><td>I</td><td>C</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Coaching and supporting Chronologue members</td><td>C</td><td>C</td><td>I</td><td>I</td><td>C</td><td>C</td><td>C</td><td>A, R</td><td>I</td>
  </tr>
  <tr>
   <td>Reviewing MRs</td><td>C</td><td>R</td><td>I</td><td>I</td><td>I</td><td>A</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Merging MRs</td><td>R</td><td>R</td><td>I</td><td>I</td><td>I</td><td>A</td><td>I</td><td>I</td><td>I</td>
  </tr>
  <tr>
   <td>Writing release notes</td><td>A, R</td><td>C</td><td>C</td><td>C</td><td>C</td><td>C</td><td>I</td><td>I</td><td>C</td>
  </tr>
  <tr>
   <td>Improve templates</td><td>C</td><td>A, R</td><td>C</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td><td>I</td>
  </tr>
</table>


### Writing process <a id="process"> </a>

The writers write in Markdown and store their documentation on the [Chronologue Documentation](https://gitlab.com/tgdp/chronologue/docs) repository on Gitlab. 
We use HUGO as a static site generator for the documentation as we regularly update the docs and it is the project's standard tool.

The Chronologue working group starts its writing process when the template is in [Phase 6 - Improve the template with user feedback](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md#improve-the-template-with-user-feedback).

We decided to come in at this late stage because developing good templates is already quite time consuming and we don't want to add to the development cycle.

The following table describes what happens after a template has been released.

| Phase | Who does it? | What happens? |
| :---  |    :----:   |   :----  |
|1 - Plan | Working group lead| <li>Add the template to the documentation plan</li><li>Create an issue to track work <li>Assign content to a writer</li>|
|2 - Draft| Writer| <li>Create a new branch off of [`main`](https://gitlab.com/tgdp/chronologue/docs)<li>Create Chronologue documentation using the template</li><li>Keep a [friction log](https://developerrelations.com/developer-experience/an-introduction-to-friction-logging) of template/guide for later improvements</li>|
|3 - Review | Writer|<li>Create MR against the `main` branch </li><li>Assign WG lead or other writer as a reviewer</li><li>Improve content based on feedback</li>|
|4 - Commentary & Publication |Writer|<li>Add commentary to your content to highlight what makes it good</li><li>Merge the MR into the `main` branch to publish the content</li>|
|5 (if applicable) - Template improvements | Writer| <li>In the Templates repository, create a new branch off of [`dev`](https://gitlab.com/tgdp/templates)</li> <li>Make changes to the template based on your friction log notes.</li><li>Create a MR and add the template author as a reviewer</li>|

### Mock product: Technical and creative needs and a proposal for tooling <a id="tools"> </a>

We want to follow good engineering practice, therefore this section separates our needs from the implementation.
That way, we can make sure that we choose appropriately for our current needs and can check in if the implementation still fits when needs change.

The Chronologue working group develops its mock tool in the [Chronologue Mock Tool](https://gitlab.com/tgdp/chronologue/mock-tool) repository. 

#### Needs:

* Framework/tool that allows for dynamic web page templating, since we want to pull in data from an API
* Needs to work with our hosting platform (Currently: Netlify OSS plan)
* Framework & knowledge support: Tool(s) need to be actively maintained and adopted by a large community. Larger projects often receive more support, and questions can be addressed by fellow community members.  
* Tool(s) need good documentation.
* Tech implementation should invite and onboard technical writers easily for maintenance later on. This can be mitigated by creating internal docs.

#### Possible technical implementation:

Since we have to build a website according to the [Chronologue mockup](https://www.figma.com/proto/lvaAChlbueycET2ws9ZquS/Chronologue?node-id=902%3A1745&scaling=min-zoom&page-id=902%3A1640&starting-point-node-id=902%3A1745), we chose the following implementation:

**Next.js** is our web development framework. 
It uses the React framework as a base, which is maintained by Meta open source. 
Furthermore, it is a mature project with extensive [documentation](https://nextjs.org/docs) and a large community. 
Next.js comes with a templating language and supports dynamic data fetching. 
It can be easily deployed through Netlify and can auto-translate to Netlify's API functions.

We are aware that deviating from our standard tech stack comes with risk. 
Ian, Chronologue's web developer, is open to experiment with data fetching, templating and page routing with HUGO. 
However, even if HUGO supports all our needs, it might not be worthwhile to switch, since development started already.
The web development team is small (1-2 people) and they would need to refactor the whole code base, delaying the deployment date. 
Opposed to the Chronologue documentation, we don't anticipate that the mock tool needs much attention after it has been published. 

There are many alternatives out there, so here is a short summary of what tools we looked at and our thoughts:

* Vue.js: similar to Next.js, but uses a different way to template and implement features like routing, data fetching, and state management. Also has enthusiast devs working on it.
* SvelteKit: relatively new player with an interesting tech implementation. Seems to miss crucial features like routing, etc. SDK not in production mode yet.
* Hugo: a static site generator works well with Netlify and has a web templating language. Unclear about routing, and data fetching.

## Consequences

### Positive impact

The proposal contributes to a better understanding of the Chronologue project’s responsibilities, ambitions, and ways of working. 
Furthermore, it establishes a firm foundation on which the working group can rely on when making future decisions. 
With this RFC, we aim to ensure a smoother process to create usable, understandable examples for people that want to use the Good Docs Project’s templates. 

### Possible negative impact

The mock tool website poses a possible risk to the maintainability of the fake tool. Since we want to divert from standard tooling (static generated site using HUGO), it can become a bottleneck if knowledgable members of the working group become unavailable. 
If we lose critical knowledge, we become less agile when it comes to resolving bugs or further development.  

### Mitigation strategy <a id="mitigation"></a>
To mitigate the risk of losing knowledgable members and becoming immobile, we want to supply comprehensive internal documentation about: 

* The framework we are working with and deviations from standard implementation (if applicable) 
* How the repository is organized
* How we approached styling (CSS) and how style files relate to the source code
* How to maintain vital parts of the website, including security updates of the framework and dependencies
* A reference document with links to more in-depth resources

Furthermore, we need to be very selective when it comes to implementing new feautres. 
The mock tool exists so we have something to write about when we test templates. 
However, if we need to document something that the mock tool can't really do, we need to come up with a solution, illustrated by the following example: 

> **Task at hand**: Test a template for a troubleshooting page. 

> **Problem**: The mock tool website is very simple. The user can only retrieve data, so there is not much to troubleshoot if you are only an end user. You can reload the page or search for a different thing, both of which are not per se tasks you need to document. 

> **Solution options**: As described in the [Function of the Chronologue](#function) section, the project consists of more than just the mock tool. We also have an API and the "physical" telescope itself to write about. So we can pick from the following solution options: 
>
>   * Pivot to a different part of the Chronologue: If we can't write troubleshooting information about the website, we may be able to write it in the context of using the API. If we can't write the docs about something tangible, we can still resort to our fictional telescope. For example, we can write a troubleshooting page for "Getting the Hadron Collider unstuck". 
>   * If the prior option is not feasible, let's say because we *need* a troubleshooting page for end users, then we would need to build a small feature that is really error prone. **However**, I would resort to this option only if really necessary. More features make the tool harder to maintain, and we want to build a stable product.


## Links and prior art

[Chronologue Figma Mockup](https://www.figma.com/proto/lvaAChlbueycET2ws9ZquS/Chronologue?node-id=902%3A1745&scaling=min-zoom&page-id=902%3A1640&starting-point-node-id=902%3A1745) created by Ulises de la Luz and Serena Jolley.

### Documentation page: Sidebars
![image.png](./image.png)


## Open questions

{This section is optional and is where you can list questions that won't be resolved by this RFC, including those raised in comments from community members.}


## Decisions deferred

While the RFC lays out what the group's purpose is and how it operates, three questions still remain to be answered as of this writing. They all touch on topics affecting other working groups as well. These decisions should be made in other RFCs.

-   **Onboarding new members**: A bottleneck in onboarding is that the writers and developers need to establish a lot of context before they can make quality contributions. It takes a lot of time to get members into a state where they can make contributions. In the past, the working group lead onboarded members personally - mostly because of the group's velocity. Since this RFC establishes a basic understanding of the group, we could develop more text-based onboarding materials, including internal documentation. 

-   **Feedback channels**: Once we publish templates and example content, and our audience interacts with them, they might want to give feedback. Instead of being angrily tweeted at, we might want to establish dedicated feedback channels. A possible solution could be an embedded form at the bottom of a page so that we get. This would allow us to get feedback specific to our content or template. However, to have a consistent UX, we should discuss how to create consistent feedback channels across our sites. 

-   **Collaboration between Templateers & Chronologuers**: The RFC states that the Chronologue writing process starts when a template is published. 
    However, there are still some unanswered questions around how the templateers and Chronologuers: 
    - How do address a need for a template which doesn't exist yet? How does a template group prioritize which templates to work on?
    - If there is a template we are testing: How do we make sure we can improve the template as smooth as possible?
    - Handover of templates over to the Chronologue: Should we have regular handover meetings? For example after a release, the groups can meet to chat about the newest templates and know what Chronologue need to focus on in the next release cycle?
    - How do the templateers want to receive feedback and how do we want to incorporate it back into what Chronologue is publishing?

## Feedback

<details><summary>Ryan's feedback: Educational puropse of Chronologue and the mimicry effect</summary>
Ryan: I have a broad concern that I had with TGDP overall: how will this serve people beyond mimicry and into genuine understanding?
Us showcasing the Chronologue as "this is good documentation" promotes mimicry, but without additional context like "we chose X direction over Y because reasons" won't educate, and risks people taking away the wrong lessons from whatever assumptions they have. What can the Chronologue do to help people understand why these docs are quality examples, and how they set out to achieve success?
Also, what's the tone & voice for the output? Since tone & voice isn't universal, are you looking to stick to one tone/voice? Are you considering a couple diverging ones, like "here's imagining a dry/professional org" and "here's what an org that plays with 'fun factors' might write?"

Felicity: Yes, and. 😁
No real solution here, but during my recent review of the Contributor's Guide template, it made me want two different examples to show how you could implement the same template quite differently -> and they'd both be equally high quality. I'm not expecting Chronologue to be able to do that, so perhaps just ignore me.

Tina (Author): I think the Chronologue documentation can not replace a thorough training or a good blog article.
Additionally, I think I left out something important of this RFC (was focused too much on the technicalities):
We DO want to annotate our content with comments that explain what makes the docs good. For example, in a procedure we chose an ordered list for steps. Oftentimes, I see non-TWs put procedures in paragraphs. So we can add a comment to the list that explains why to prefer an ordered list.
Regarding voice and tone: These are quite intricate levels of writing, and I am not sure if they matter all too much to someone "who just wants to ship their docs".
I feel like voice and tone differ from project to project and we can certainly try to educate users about the pros and cons of which tone, but I'm not sure if Chronologue is the best place for that.
For the Chronologue docs, I am assuming we want to have a consistent voice and tone that follows whatever the styleguide group mandates.
FYI: @peterjurich

Felicity: My Content-Strategy-spidey-senses are tingling. Chronologue can't do everything to showcase the templates. So you're going to show one kind of implementation. According to the Chronologue style guide.
Then, what we could do (I'm sensing another working group) is have people create examples of using our templates in different styles. These could be showcased on the website. (side thought: probably we need to create a content ecosystem map 🤔) These would fulfil the 'educational' aspect. Say: different project sizes (startup vs. massive); or different products (software vs hardware); or different levels of complexity.


Alyssa: My two cents is that let's not let the perfect be the enemy of good here. Let's also not try to solve a problem that we don't yet have. Let's focus on getting a simple MVP/MVD and adding to that. One example is better than no examples.
The other thing to keep in mind is that the Chronologue is our fictional tool and we can make it multi-faceted enough to showcase different implementations. Or we simply state up front what is and isn't out of scope. And perhaps our website (the platform for consuming the templates and the likely jump point to the Chronologue docs for examples) can supplement the explanations and examples. Or the templates themselves can.

Peter: Hey all! While I understand the importance of them, my biggest concern with voice and tone is the risk of stifling anyone's creativity or vision for how their specific assignment should look. I'm sure there are tactful ways to have this discussion that we can have down the road, but I agree with Tina and Alyssa here: The Chronologue may not be the best place for this, and it seems like we'd be getting ahead of ourselves to focus on it.
My thoughts are we establish a few docs that we're currently working on, go through them as a group to see if there are common themes in the voice. If there are, lean into them or otherwise have a discussion around them. Either way, it feels like a problem for tomorrow.
</details>


## Implementation checklist

If this proposal is accepted, the following tasks must be completed:

- [ ] Create detailed documentation for our infrastructure on the `main` branch, see [Mitigation strategy](#mitigation).

## Votes

Votes as per our [decision process](https://thegooddocsproject.dev/decisions/):

Project steering committee (listed alphabetically by first name):

- Aaron Peters: +1
- Alyssa Rock: +1
- Ankita Tripathi:
- Bryan Klein: +1
- Cameron Shorter: +0
- Carrie Crowe: +1
- Deanna Thompson: +1 
- Erin McKean:
- Felicity Brand: +1
- Gayathri Krishnaswamy: +1
- Morgan Craft:
- Nelson Guya:
- Ryan Macklin: +1
- Tina Lüdtke: +1


Community members who voted (non-binding):

- {Your name}: {Your vote}
